import React, { useState, useEffect } from 'react';
import axios from 'axios';

// Reusar logica
const useResources = (resourceName) => {
  const [resources, setResources] = useState([]);

  useEffect(() => {
    fetchResources(resourceName);
  }, [resourceName]);

  const fetchResources = async (resourceName) => {
    setResources([]);
    const { data } = await axios.get(
      `https://jsonplaceholder.typicode.com/${resourceName}`
    );

    setResources([...data]);
  };

  return resources;
};

export default useResources;
