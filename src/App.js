import React, { useState } from 'react';
import ResourceList from './ResourceList';
// App Terminado
function App() {
  const [resourceName, setResourceName] = useState('posts');

  return (
    <>
      <button onClick={() => setResourceName('posts')}>Posts</button>
      <button onClick={() => setResourceName('todos')}>Todos</button>
      <button onClick={() => setResourceName('users')}>Users</button>
      <ResourceList resourceName={resourceName}></ResourceList>
    </>
  );
}

export default App;
