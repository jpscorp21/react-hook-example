import React from 'react';
import useResources from './useResources';

const ResourceList = ({ resourceName }) => {
  const resources = useResources(resourceName);

  if (!resources.length) {
    return <p>Cargando...</p>;
  }

  if (resourceName == 'users') {
    return (
      <ul>
        {resources.map((r) => (
          <li key={r.id}>{r.name}</li>
        ))}
      </ul>
    );
  }

  return (
    <ul>
      {resources.map((r) => (
        <li key={r.id}>{r.title}</li>
      ))}
    </ul>
  );
};

export default ResourceList;
